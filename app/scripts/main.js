$(document).ready(function() {

	$(".fancybox")
	.attr('rel', 'gallery')
	.fancybox({
		padding: 0,
		prevEffect : 'none',
		nextEffect : 'none',
		closeEffect	: 'none',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(221, 221, 221, 0.80)'
				}
			}
		}
	});

	imgScale();
	// if ($(window).width() < 1200) {
	// 	$(window).resize(function () {
	// 		imgScale();
	// 	});
	// 	$(window).resize();
	// }

	stopOverSlider('.slider-1');

	$('.slider-1').owlCarousel({
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true,
		navigation : true,
		loop:true,
		items:1,
		margin:0,
		stagePadding:0,
		smartSpeed:1000,

		onInitialized: dotWork
	});

	$('.slider-2').owlCarousel({
		loop:true,
		margin:20,
		nav:true,
		dots:false,

		onInitialized: imgScale,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			640:{
				items:2
			},
			960:{
				items:3
			},
			1200:{
				items:4
			}
		}
	});

	$(document).on('click', '.nav-trigger', function(){
		$(this).toggleClass('active');
		$('.top-nav').slideToggle(200);
	});

	$(document).on('click', '.top-widget__login', function(){
		$(this).toggleClass('active');
		$('.login-widget').slideToggle(200);
	});
	$(document).click(function(e) {
		var $trg = $(e.target);

		if ( !$trg.closest('.login-widget').length && !$trg.hasClass('top-widget__login') ) {
			$('.top-widget__login').removeClass('active');
			$('.login-widget').slideUp(200);
		}
		if ( $(window).width() < 1200 ) {
			if ( !$trg.closest('.top-nav').length && !$trg.hasClass('nav-trigger') ) {
				$('.nav-trigger').removeClass('active');
				$('.top-nav').slideUp(200);
			}
		}

	});

	showService('.s-item__view');

	$(document).on('click', '.s-item__info-close', function(e){
		e.preventDefault();
		var $t = $(this);
		$('.s-item__view').removeClass('active');
		$t.closest('.s-item__info').addClass('invis');
		$t.closest('.s-item').removeAttr('style');
	});

	dragFoto();
});
/* for slider-1 */
function dotWork() {
	var $t = $('.owl-dot');
	$t.removeClass('active');
	setTimeout(function() {
		$t.first().addClass('active');
	}, 10);
}

function stopOverSlider(sliderClass) {
	$(document).on('mouseenter', sliderClass, function (e) {
		$(this).find('.owl-dot.active').addClass('stay-active');
	});
	$(document).on('mouseleave', sliderClass, function (e) {
		$(this).find('.owl-dot').removeClass('stay-active');
	});
}
function imgScale() {
	$("img.scale").imageScale();
}
/* .for slider-1 */

function accordion (spoilerTitle, accordionTrue){
	$(document).on('click', spoilerTitle, function(e) {
		var
		$t = $(this),
		$item = $t.parent(),
		$itemClass = $item.attr('class'),
		$spoiler = $t.next(),
		$spoilerClass = $spoiler.attr('class')
		;

		e.preventDefault();

		if (accordionTrue) {
			$('.'+$spoilerClass).slideUp(200);
		};

		if ($item.hasClass('active')) {
			$spoiler.slideUp(200);
			$item.removeClass('active');
		} else {

			if (accordionTrue) {
				$('.'+$itemClass).removeClass('active');
			} else {
				$item.removeClass('active');
			}

			$item.addClass('active');
			$spoiler.slideDown(200);

		}
	});
}

function initMap() {
	var myLatLng = {
		lat: 55.821481,
		lng: 37.72787
	};
	var map = new google.maps.Map(document.getElementById('map-init'), {
		zoomControl: false,
		mapTypeControl: false,
		panControl: false,
		streetViewControl: false,
		center: myLatLng,
		scrollwheel: false,
		zoom: 16
	});

	var marker = new google.maps.Marker({
		map: map,
		position: myLatLng,
		title: 'г. Москва, ул. Пермская, д.11, стр.5'
	});
}

function showService(selector) {
	$(document).on('click', selector, function (e) {

		var
		$t = $(this)
		,$href = $t.attr('href')
		,$contentWrapper = $t.next()
		,$contClass = $('.s-item__info')
		;


		e.preventDefault();
		if ( !$t.hasClass('active') ) {

			$(selector).removeClass('active');
			$(selector).parent().removeAttr('style');
			$t.addClass('active');
			$contClass.addClass('invis');
			setTimeout(function () {
				$contClass.removeClass('loaded');
				loadContent($contentWrapper, $href);
				scrollInView();
			}, 400);
		} else{
			$t.removeClass('active');
			$(selector).parent().removeAttr('style');
			$contentWrapper.addClass('invis')
		}
	});
}
function loadContent(divIn, href) {
	if (!divIn.hasClass('loaded')) {
		$.ajax({
			url: href,
			type: 'GET',
			success: function (data) {
				divIn.addClass('loaded')
				.html( data )
				.removeClass('invis');

				var
				$inner = divIn.find('.s-item__info-inner')
				,$innerHeight = $inner.height()
				;
				$inner.parent().height($innerHeight);
				divIn.parent().height(function (index, height) {
					return (height + $innerHeight);
				});
			}
		});
	} else {
		console.log('already loaded');
		return;
	};
}
function scrollInView(){
	$('html, body').animate({
		scrollTop: parseInt($('.s-item__view.active').offset().top - 20, 10)
	}, 600 );
}

function dragFoto() {

	var $dropzone = $('.dropzone');
	var fileInput = document.getElementById("fileInput");
	var output = document.getElementById("result");
	var delBtn = document.getElementById("delBtn");
	var okBtn = document.getElementById("okBtn");
	var rotateBtn = document.getElementById("rotateBtn");
	var crop = null;
	var cropResult = null;
	var isAdvancedUpload = function() {
		var div = document.createElement('div');
		return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
	}();

	if ($dropzone.length == 0) {
		return false;
	}

	if (isAdvancedUpload) {
		var fr = new FileReader();
		$dropzone
		.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
			e.preventDefault();
			e.stopPropagation();
		})
		.on('dragover dragenter', function() {
			$dropzone.addClass('is-dragover');
		})
		.on('dragleave dragend drop', function() {
			$dropzone.removeClass('is-dragover');
		})
		.on('drop', function(e) {
			fileInput.files = e.originalEvent.dataTransfer.files;
		});
	} else {
		alert('Ваш браузер не поддерживает Drag\'n\'drop, нажмите на область для фотографии чтобы загрузить фото');
	}

	fileInput.addEventListener("change", function(e) {

		var files = e.target.files;

		delBtn.classList.add('visible');
		okBtn.classList.add('visible');
		rotateBtn.classList.add('visible');

		for (var i = 0; i < files.length; i++) {
			var file = files[i];

			if (!file.type.match('image'))
				continue;

			var picReader = new FileReader();

			picReader.addEventListener("load", function(e) {


				var picFile = e.target;
				var $croppiePlace = $(output);

				output.classList.add('img-loaded');
				$dropzone.addClass('is-dropped customizable');

				crop = $croppiePlace.croppie({
					viewport: {
						width: 245,
						height: 335,
						type: 'square'
					},
					boundary: {
						width: $croppiePlace.parent().outerWidth(),
						height: $croppiePlace.parent().outerHeight()
					},
					enableOrientation: true
				});
				crop.croppie('bind', {
					url: picFile.result,
					orientation: 1
				});

			});

			picReader.readAsDataURL(file);
		}

	});

	function showResult(result) {
		var html;
		if (result.html) {
			html = result.html;
		}
		if (result.src) {
			html = '<img src="' + result.src + '" />';
			output.innerHTML = html;
			output.classList.remove('croppie-container');
			output.classList.add('result-shown');
		}
	}

	okBtn.addEventListener('click', function(e){
		e.preventDefault();
		e.target.classList.remove('visible');
		rotateBtn.classList.remove('visible');
		$dropzone.removeClass('customizable');

		crop.croppie('result', {
			type: 'canvas',
			size: {
				width: $(output).parent().width(),
				height: $(output).parent().height()
			}
		}).then(function(src) {
			showResult({
				src: src
			});
		});

	});

	rotateBtn.addEventListener('click', function(e){
		e.preventDefault();
		crop.croppie('rotate', parseInt( $(this).data('deg') ));
	});

	delBtn.addEventListener('click', function(e){
		e.preventDefault();
		fileInput.value = null;
		e.target.classList.remove('visible');
		okBtn.classList.remove('visible');
		rotateBtn.classList.remove('visible');
		$(output).removeClass('img-loaded result-shown');
		$dropzone.removeClass('is-dropped customizable');
		output.innerHTML = '';
	});
}